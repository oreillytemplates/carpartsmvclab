<%@ include file="/WEB-INF/layouts/include.jsp"%>

<nav class="navbar bg-light">
	<!-- Links -->
	<!-- <ul class="navbar-nav"> -->
	<ul class="nav nav-pills flex-column">
<%-- 		<li>
		    <a class="${active eq 'add' ? 'active nav-link' : 'nav-link'}" 
		       href="<c:url value='/carparts/partnumber' />">
		    	Add Part Number
		    </a>
		</li> --%>
		<li class="nav-item">
			<a class="${active eq 'H4666ST' ? 'active nav-link' : 'nav-link'}" 
			   href="<c:url value='/carparts/electrical/H4666ST'/>">
				Headlight Bulb
			</a>
		</li>
		<li class="nav-item">
			<a class="${active eq 'M134HV' ? 'active nav-link' : 'nav-link'}" 
			   href="<c:url value='/carparts/engine/M134HV' />">
				Oil Pumps
			</a>
		</li>
		<li class="nav-item">
			<a class="${active eq '40026' ? 'active nav-link' : 'nav-link'}" 
			   href="<c:url value='/carparts/other/40026' />">
				Air Pump
			</a>
		</li>
<%-- 		<li>
			<a class="${active eq 'regular' ? 'active nav-link' : 'nav-link'}" 
		   	   href="<c:url value='/carparts/regular' />">
				Feedback
			</a>
		</li>
		<li>
			<a class="${active eq 'contactus' ? 'active nav-link' : 'nav-link'}" 
		   	   href="<c:url value='/carparts/contactus' />">
				Contact Us
			</a>
		</li>
		<li>
			<a class="${active eq 'emailus' ? 'active nav-link' : 'nav-link'}" 
		   	   href="<c:url value='/carparts/emailus' />">
				Email Us
			</a>
		</li> --%>
	</ul>
</nav>