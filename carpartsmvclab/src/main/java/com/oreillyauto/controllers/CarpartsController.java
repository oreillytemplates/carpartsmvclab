package com.oreillyauto.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import com.oreillyauto.service.CarpartsService;

@Controller
public class CarpartsController {

	@Autowired
	CarpartsService carpartsService;

	// TODO: Remove @ResponseBody and implement this method
	@ResponseBody
    @GetMapping(value = { "/carparts/partnumber" })
    public String getPartnumber(Model model, String partnumber) throws Exception {
		return "Not implemented";
    }

	// TODO: Remove @ResponseBody and implement this method
	@ResponseBody
	@GetMapping(value = {"/carparts/electrical/{partNumber}"})
	public String getElectrical(@PathVariable String partNumber, Model model) throws Exception {
		return "Not implemented";
	}
	
	// TODO: Remove @ResponseBody and implement this method
	@ResponseBody
	@GetMapping(value = {"/carparts/engine/{partNumber}"})
	public String getEngine(@PathVariable String partNumber, Model model) throws Exception {
		return "Not implemented";
	}
	
	// TODO: Remove @ResponseBody and implement this method
	@ResponseBody
	@GetMapping(value = {"/carparts/other/{partNumber}"})
	public String getOther(@PathVariable String partNumber, Model model) throws Exception {
		return "Not implemented";
	}

	// TODO: Remove @ResponseBody and implement this method
	@ResponseBody
	@GetMapping(value = { "/carparts/carpartManager" })
	public String getCarpartManager(Model model) {
		return "Not implemented";
	}

	@GetMapping(value = { "/carparts" })
	public String carparts(Model model, String firstName, String lastName) throws Exception {


		return "carparts";
	}

}
