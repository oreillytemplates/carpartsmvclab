package com.oreillyauto.controllers;

import org.apache.log4j.Logger;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IntroController {
    
    private static final Logger log = Logger.getLogger(IntroController.class);

    @RequestMapping(value = {"/","/index"})
    public String index() {
        return "index";
    }

    @PreAuthorize("hasRole('USER')")
    @RequestMapping(value = "/home")
    public String home() {
        return "home";
    }
    
}
