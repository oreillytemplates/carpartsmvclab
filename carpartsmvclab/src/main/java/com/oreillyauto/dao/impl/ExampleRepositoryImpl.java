package com.oreillyauto.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import com.oreillyauto.dao.custom.ExampleRepositoryCustom;
import com.oreillyauto.domain.examples.Example;
import com.oreillyauto.domain.examples.QExample;
import com.oreillyauto.domain.examples.QNickname;
import com.querydsl.core.Tuple;
import com.querydsl.core.group.GroupBy;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.impl.JPADeleteClause;
import com.querydsl.jpa.impl.JPAUpdateClause;

@Repository
public class ExampleRepositoryImpl extends QuerydslRepositorySupport implements ExampleRepositoryCustom {
	// Add your queries here. Typed or QueryDSL...
	
	private QExample exampleTable = QExample.example;
	private QNickname nicknameTable = QNickname.nickname;
	
    public ExampleRepositoryImpl() {
        super(Example.class);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<Example> getExamples() {
		List<Example> eventList = (List<Example>) (Object) getQuerydsl().createQuery()
                .from(exampleTable)
                .fetch();
        return eventList;
    }
    
    @SuppressWarnings("unchecked")
    public void printExampleTable() {
        QExample exampleTable = QExample.example;
        
        List<Example> list = (List<Example>) (Object) getQuerydsl().createQuery()
                .from(exampleTable)
                .fetch();
        
        for (Object result : list) {
            System.out.println(result);
        }
    }
    
    public void printList(List<?> list) {
        for (Object result : list) {
            System.out.println(result);
        }
    }

    public void printList(String note, List<?> list) {
    	System.out.println(note);
        for (Object result : list) {
            System.out.println(result);
        }
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<Example> getExampleById(Integer id) {
        String sql = "SELECT * " + 
                     "  FROM examples " + 
                     " WHERE id = ?";
        Query query = getEntityManager().createNativeQuery(sql);
        query.setParameter(1, id); // NOTICE PARAMETERS ARE "1-BASED" and not "0-based"
        return (List<Example>)query.getResultList();
    }
    
    
    public Example getFirstExample() {
    	QExample exampleTable = QExample.example;
		Example example = (Example) 
				from(exampleTable)
				.limit(1)
				.fetch();
        return example;
    }
    
    @SuppressWarnings("unchecked")
    public List<Example> getAllExamples() {
    	QExample exampleTable = QExample.example;
		List<Example> eventList = (List<Example>) (Object) getQuerydsl().createQuery()
                .from(exampleTable)
                .fetch();
        return eventList;
    }
	
	@Override
	public void testQueries(String day) {
		switch (day) {
		case "3":
			testDayThree();
			break;
		case "4":
			testDayFour();
			break;
		}
	}
    
	@SuppressWarnings("unchecked")
	private void testDayFour() {
		
		// INSERT Example - Typed SQL
		// Since we deleted all users with last_name = "Brannon" yesterday,
		// let's create a new one.
        String sql = "INSERT INTO examples (first_name, last_name) " + 
                     "VALUES ('Jeffery', 'Brannon') ";
        Query query = getEntityManager().createNativeQuery(sql);
        query.executeUpdate();
		
		
/**
	ResultTransformer
	Querydsl provides a meaningful way to customize results using the ResultTransformer for aggregation. 
	The transformer works directly with the group by function to transform the data in memory. When 
	working with group by, realize that we are working with aggregate functions and therefore end up 
	with objects that do not necessarily relate to each other in any way (i.e. they are not necessarily 
	columns we would find in the table we are querying - like AVG). This list of potentially disjointed
	data is a Tuple. 
 */
		
        // Tuple example using a group by 
		// exampleTable.count() is our aggregate
        List<Tuple> list = (List<Tuple>) (Object) getQuerydsl().createQuery()
            .select(exampleTable.lastName, exampleTable.count())
            .from(exampleTable)
            .groupBy(exampleTable.lastName)
            .fetch();
        
        printList(list);
		
		
/**
	Query for Map using the Transform function
	Since we are working with aggregate data, we can transform the data into a java.util.Map, given 
	that the aggregate column can become the key and hold some value.
 */
        // Map - Transform Example - Group By Last Name
        Map<String, Example> exampleMap = getQuerydsl().createQuery()
                .from(exampleTable)
                .where(exampleTable.id.in(
                    JPAExpressions.select(exampleTable.id)
                    .from(exampleTable)
                    ))
                .transform(GroupBy.groupBy(exampleTable.lastName).as(exampleTable));
        
        for (Map.Entry<String,Example> entry : exampleMap.entrySet()) {  
            System.out.println("Key = " + entry.getKey() + 
                             ", Value = " + entry.getValue());
        }
       
        
	}

	@SuppressWarnings("unchecked")
	public void testDayThree() {
		/** Some things you should know about casting in Java before we begin */
		// We can cast from List to Object in Java 
    	List<Integer> list = new ArrayList<Integer>();
    	list.add(1);
    	
    	//Integer myInteger = (Integer)list;  Cannot cast from List<Integer> to Integer
    	// Nope!
    	
    	// EXAMPLE 1 -----------------------------------------------------------------
    	// Simple example - retrieve one record 
    	// Notice that we are calling the method getQuerydsl() (in the QuerydslRepositorySupport object) 
    	// and then we are calling the method createQuery()
    	Example example = (Example) getQuerydsl().createQuery()
    	           .from(exampleTable)
    	           .limit(1)
    	           .fetchOne();
    	System.out.println("Example 1: " + example);
    	
    	// EXAMPLE 2 -----------------------------------------------------------------
    	// Simple example - retrieve one record 
    	// Notice that we removed the two method calls
    	Example example2 = (Example) 
    	           from(exampleTable)
    	           .limit(1)
    	           .fetchOne();
    	System.out.println("Example 2: " + example);
    	
    	
        // EXAMPLE 3 -----------------------------------------------------------------
		List<Example> eventList = (List<Example>) (Object) getQuerydsl().createQuery()
                .from(exampleTable)
                .fetch();
        // printList(eventList);
    	
        
		
		// EXAMPLE 4 -----------------------------------------------------------------
		// TYPED SQL
        // java.persistence.Query takes a SQL string expression "SELECT * FROM <table>";
        String sql = "SELECT * " + 
                     "  FROM examples ";
        Query query = getEntityManager().createNativeQuery(sql);
        List<Example> exampleList = (List<Example>)query.getResultList();
        //printList("Example 4", eventList);
        
        
        // EXAMPLE 5 - Select One "Jeffery"  -------------------------------------------
        Example example77 = (Example) getQuerydsl().createQuery()
                .from(exampleTable)
                .where(exampleTable.firstName.eq("Jeffery"))
                .fetchOne();
        //printList("Example 5", eventList);
        
        // EXAMPLE 6 - Select users with first name = "Jeffery" ------------------------
        List<Example> jefferyList = (List<Example>) (Object) getQuerydsl().createQuery()
                .select(exampleTable.id, exampleTable.firstName, exampleTable.lastName)
                .from(exampleTable)
                .where(exampleTable.firstName.eq("Jeffery"))
                .fetch();
        // print!
                
        // Example 7 - Multiple sources (tables) ----------------------------------------
        List<Object> exampleList3 = (List<Object>) (Object) getQuerydsl().createQuery()
                .select(exampleTable.firstName, exampleTable.lastName, nicknameTable.nickName)
                .from(exampleTable)
                .innerJoin(nicknameTable)
                .on(exampleTable.id.eq(nicknameTable.id))
                .fetch();
        // print!
        
        
        // Example 8 - Multiple Filters -------------------------------------------------
        List<Object> exampleList4 = (List<Object>) (Object) getQuerydsl().createQuery()
                .select(exampleTable.firstName, exampleTable.lastName, nicknameTable.nickName)
                .from(exampleTable)
                .innerJoin(nicknameTable)
                .on(exampleTable.id.eq(nicknameTable.id))
                .where(nicknameTable.nickName.eq("Giraffe"), (nicknameTable.nickName.eq("Nate Dawg")))
                .fetch();  // Comma in the predicate is an "or"
        // print!
        
        
        // Example 9 - Multiple Filters --------------------------------------------------
        List<Object> exampleList4a = (List<Object>) (Object) getQuerydsl().createQuery()
                .select(exampleTable.firstName, exampleTable.lastName, nicknameTable.nickName)
                .from(exampleTable)
                .innerJoin(nicknameTable)
                .on(exampleTable.id.eq(nicknameTable.id))
                .where(
                		     nicknameTable.nickName.eq("Giraffe")
                		 .or((nicknameTable.nickName.eq("Nate Dawg")))
                	  ) // another example using or
                
                .fetch();
        // print!
        
        
        
        // Example 10 - Left Join ----------------------------------------------------------
        List<Object> exampleList5 = (List<Object>) (Object) getQuerydsl().createQuery()
                .select(exampleTable.firstName, exampleTable.lastName, nicknameTable.nickName)
                .from(exampleTable)
                .leftJoin(nicknameTable)
                .on(exampleTable.id.eq(nicknameTable.id))
                .fetch();
        // print!
        
        

        // Example 11 - Ordering -----------------------------------------------------------
        List<Example> exampleList6 = (List<Example>) (Object) getQuerydsl().createQuery()
                .select(exampleTable.firstName, exampleTable.lastName)
                .from(exampleTable)
                .orderBy(exampleTable.firstName.asc())
                .fetch();
        // print!
        
        
        // Example 12 - Grouping -----------------------------------------------------------
        List<Example> exampleList7 = (List<Example>) (Object) getQuerydsl().createQuery()
                .select(exampleTable.lastName)
                .from(exampleTable)
                .groupBy(exampleTable.lastName)
                .fetch();
        // print!
        
        // Example 13 - Delete -------------------------------------------------------------
        EntityManager em = getEntityManager();
        em.joinTransaction();
        long deletedItemsCount = new JPADeleteClause(em, exampleTable)
                .where(exampleTable.lastName.eq("Brannon"))
                .execute();
        
        //System.out.println(deletedItemsCount + " records deleted.");

        
        
        // Example 14 - Delete -------------------------------------------------------------
        // Now that we have potentially deleted some records, let's query the entire
        // table and print the results
        List<Example> exampleList8 = (List<Example>) (Object) getQuerydsl().createQuery()
                .from(exampleTable)
                .fetch();
        
        // print!
        
        

        // Example 15 - Update ------------------------------------------------------------
        long updatedCount = new JPAUpdateClause(getEntityManager(), exampleTable)
                .where(exampleTable.lastName.toLowerCase().eq("sutton"))
                .set(exampleTable.lastName, "Dawg")
                .execute();
//            System.out.println(updatedCount + " records updated.");
//            printExampleTable();

        
        
        
         // Example 16 - Sub Query ---------------------------------------------------------
        Object obj = getQuerydsl().createQuery()
                .from(exampleTable)
                .where(exampleTable.id.in(
                    JPAExpressions.select(exampleTable.id.max())
                    .from(exampleTable)
                    ))
                .fetchOne();    
            
        //System.out.println("obj=> "  + obj);
        
	}
	
    @SuppressWarnings("unchecked")
    @Override
    public List<Object> getTables() {
        String sql = "  SELECT st.tablename " + 
        	  	     "    FROM sys.systables st " + 
        		     "   WHERE st.tablename NOT LIKE 'SYS%'" +
        		     "ORDER BY st.tablename";
        Query query = getEntityManager().createNativeQuery(sql);
        return (List<Object>)query.getResultList();
    }
}

