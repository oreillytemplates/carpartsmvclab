package com.oreillyauto.dao.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import com.oreillyauto.dao.custom.CarpartsRepositoryCustom;
import com.oreillyauto.domain.carparts.Carpart;
import com.oreillyauto.domain.carparts.QCarpart;

@Repository
public class CarpartsRepositoryImpl extends QuerydslRepositorySupport implements CarpartsRepositoryCustom {

	QCarpart carpartTable = QCarpart.carpart;
	
	public CarpartsRepositoryImpl() {
		super(Carpart.class);
	}

	// TODO: Return list of carparts using TYPED SQL
	@SuppressWarnings("unchecked")
	@Override
	public List<Carpart> getCarparts() {
		return Collections.EMPTY_LIST;
	}
	
	@Override
	public Carpart getCarpartByPartNumber(String partNumber) {	
		Carpart carpart = new Carpart();

		// TODO Fake it 'til you make it code goes here
		
		return carpart;
	}

	private Map<String, String> getAirPumpDetails() {
		Map<String, String> detailsMap = new HashMap<String, String>();
		detailsMap.put("Maximum Pressure (psi)", "150 psi");
		detailsMap.put("Voltage (V)", "120 Volt");
		return detailsMap;
	}

	private Map<String, String> getOilPumpDetails() {
		Map<String, String> detailsMap = new HashMap<String, String>();
		detailsMap.put("New Or Remanufactured", "New");
		detailsMap.put("Gasket Or Seal", "No");
		detailsMap.put("Pickup Screen Included", "Yes");
		return detailsMap;
	}

	@Override
	public Carpart getCarpart(String partNumber) {
		return null;
	}

}
