package com.oreillyauto.service.impl;

import java.util.Collections;
import java.util.List;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oreillyauto.dao.CarpartsRepository;
import com.oreillyauto.domain.carparts.Carpart;
import com.oreillyauto.service.CarpartsService;

@Service("carpartsService")
public class CarpartServiceImpl implements CarpartsService {
	@Autowired
	CarpartsRepository carpartsRepo;
	
	// TODO: save the carpart pass in
	@Override
	public void saveCarpart(Carpart carpart) {
		
	}
	
	// TODO: Get Carpart By Part Number from your repo
    @Override
    public Carpart getCarpartByPartNumber(String partNumber) throws Exception {    	
    	return null;
    }

    // TODO: return list of carparts using CRUD REPO
	@Override
	public List<Carpart> getCarparts() {
		return Collections.EMPTY_LIST;
	}

	// TODO: delete carpart using CRUD REPO
	@Override
	public void deleteCarpartByPartNumber(Carpart carpart) {
	    
	}

}
