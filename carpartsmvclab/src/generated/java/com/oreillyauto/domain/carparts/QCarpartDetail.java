package com.oreillyauto.domain.carparts;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QCarpartDetail is a Querydsl query type for CarpartDetail
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QCarpartDetail extends EntityPathBase<CarpartDetail> {

    private static final long serialVersionUID = 618542470L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QCarpartDetail carpartDetail = new QCarpartDetail("carpartDetail");

    public final QCarpart carpart;

    public final NumberPath<Integer> detailId = createNumber("detailId", Integer.class);

    public final StringPath detailKey = createString("detailKey");

    public final StringPath detailValue = createString("detailValue");

    public QCarpartDetail(String variable) {
        this(CarpartDetail.class, forVariable(variable), INITS);
    }

    public QCarpartDetail(Path<? extends CarpartDetail> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QCarpartDetail(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QCarpartDetail(PathMetadata metadata, PathInits inits) {
        this(CarpartDetail.class, metadata, inits);
    }

    public QCarpartDetail(Class<? extends CarpartDetail> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.carpart = inits.isInitialized("carpart") ? new QCarpart(forProperty("carpart")) : null;
    }

}

